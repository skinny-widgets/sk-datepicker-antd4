
import { AntdSkDatepicker }  from '../../sk-datepicker-antd/src/antd-sk-datepicker.js';


export class Antd4SkDatepicker extends AntdSkDatepicker {

    get prefix() {
        return 'antd4';
    }

}
